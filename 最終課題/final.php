<?php
	//置き換え
	function replace($text)
	{
		$text = str_replace(PHP_EOL,"^<br^>",$text);	//改行コード置き換え
		$text = str_replace(" ", "+",$text);			//半角スペース置き換え
		$text = str_replace("　", "%E3%80%80",$text);	//全角スペース置き換え
		$text = str_replace("	", "%09",$text);		//水平タブ置き換え		
		
		return $text;
	}
?>

<?php
	
	//final.phpのいる場所のパス
	$nowPath = __DIR__;
	
	//コマンドプロンプトからの実行の場合、コマンドプロンプトのcdをfinal.phpと同じに変更
	chdir($nowPath);
	
	//タイムゾーン指定
	date_default_timezone_set('Asia/Tokyo');
	
	//移動したいhtmlファイルのフルパス
	$path = realpath('final.htm');
	
	//URLに変更（前提としてローカルに所属しているものとする）
	$path = "http://localhost" . substr($path,2);
	
	//300秒間隔に繰り返し
	while(true){
		
		//ファイル指定
		$dir = "./monitor/";
		
		//ファイル探査
		foreach(glob($dir . "{*.txt,.txt}", GLOB_BRACE) as $file)
		{
			
			//---------出力データ準備-------------------------------//
																	//
			//ファイル名確保										//
			$name = basename($file);								//
																	//
			//ファイルのタイムスタンプ確保							//
			$date = replace(date("Y-m-d h:i:s",filemtime($file)));	//
																	//
			//テキストファイル内のテキスト確保						//
			$fp = fopen($file,'r');									//
																	//
			//テキストデータ初期化									//
			$str = "";												//
																	//
			//ファイルの末尾まで探査								//
			while(!feof($fp))										//
			{														//
				$str .= replace(fgets($fp));						//
			}														//
			//------------------------------------------------------//
			
			//ファイルリンク解除
			fclose($fp);
			
			//ファイル削除
			unlink($file);
			
			//htmlファイルにデータ送信
			shell_exec("start " . $path . "?TimeStanp=" . $date . "^&FileName=" . $name . "^&FileStr=" . $str);
			
			//報告
			echo "ファイル名：" . $name . " を削除しました。\n";
		}
		
		//現在時刻
		echo "現在時刻：" . date("Y-m-d H:i:s") . "\n";
		

		//間隔300
		sleep(300);
	}

?>