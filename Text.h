#pragma once
#include "Direct2D.h"

namespace Text
{
	struct Point
	{
		float X;
		float Y;
	};

	int SetText(std::string text, std::string fontType, Point point,float fontSize, D2D1::ColorF::Enum color);

	void ChangeText(int handle, std::string text);

	void ChangePoint(int handle, Point point);

	void ChangeColor(int handle,D2D1::ColorF::Enum color);

	void ChangeFormat(int handle, std::string fontType, float fontSize);

	D2D1::ColorF::Enum ConvertColor(unsigned char red, unsigned char gleen, unsigned char blue);

	void Visible(int handle);

	void Invisible(int handle);

	void Draw();

	void Release();
}