#pragma once

#include <iostream>
#include <time.h>

using namespace std;

//立幅
#define tate 15

//横幅
#define yoko 17

//0..通路　1..壁　2..スタート地点　3..ゴール地点
#define road 0
#define wall 1
#define start 2
#define goal 3

#define dir00puls subPoint[0] + direction[Rand][0]
#define dir01puls subPoint[1] + direction[Rand][1]

#define dir00puls2 subPoint[0] + 2 * direction[Rand][0]
#define dir01puls2 subPoint[1] + 2 * direction[Rand][1]

//マップ
int maze[tate][yoko];

//スタート地点
int start_[2] = { tate - 3,1 };

//ゴール地点
int goal_[2] = { tate - 3,yoko - 2 };

//方向
//右、下、左、上
int direction[4][2] = { {0,1},{1,0},{0,-1},{-1,0} };

//注目点
int point[2] = { 2,4 };

//注目点作成
void makePoint()
{
	//確保用
	int tatePoint = rand() % tate;
	int yokoPoint = rand() % yoko;

	//偶数じゃなければ
	if (tatePoint / 2)
		//偶数にする
		tatePoint -= 1;

	//偶数じゃなければ
	if (yokoPoint / 2)
		//偶数にする
		yokoPoint -= 1;

	//注目点更新
	point[0] = tatePoint;
	point[1] = yokoPoint;
}

//初期化
void Initialize()
{
	//壁化
	for (int i = 0; i < tate; i++)
	{
		for (int j = 0; j < yoko; j++)
		{
			maze[i][j] = wall;
		}
	}

	//外周通路化
	for (int i = 0; i < tate; i++)
	{
		maze[i][0] = road;
		maze[i][yoko - 1] = road;
	}
	for (int j = 0; j < yoko; j++)
	{
		maze[0][j] = road;
		maze[tate - 1][j] = road;
	}

	//注目点壁化
	maze[point[0]][point[1]] = road;
}

//表示部
void Open()
{
	//スタート地点とゴール地点を登録
	maze[start_[0]][start_[1]] = start;
	maze[goal_[0]][goal_[1]] = goal;

	for (int i = 0; i < tate; i++)
	{
		for (int j = 0; j < yoko; j++)
		{
			//壁か道かスタートかゴールか
			switch (maze[i][j])
			{
			case wall:
				cout << "■";
				break;
			case road:
				cout << "　";
				break;
			case start:
				cout << "ス";
				break;
			case goal:
				cout << "ゴ";
				break;
			}
		}
		cout << endl;
	}
}

//堀ロリ
void Dig(int x, int y)
{
	//注目点設定
	int subPoint[2];

	//注目点
	subPoint[0] = x;
	subPoint[1] = y;

	//Rand...方向管理用ランダム変数
	//flg...くり返し用フラグ
	//cap...回数管理
	int Rand = 0, flg = 0, cap = 0;
	
	//行った方向管理
	int RandArray[4] = { 4,4,4,4 };

	//くり返し
	while (true)
	{
		//フラグ初期化
		flg = 0;

		//制限回数超えてたら終了
		if (cap > 3)break;

		//方向設定
		Rand = (int)rand() % 4;
		
		//すでに行ったかチェック
		for (int i = 0; i < 4; i++)
		{
			if (RandArray[i] == Rand)
			{
				flg = 1;
				break;
			}
		}

		//行ってたらもっかい
		if (flg == 1)continue;

		//行ってなかったらログに登録
		else {
			RandArray[cap] = Rand;
			cap++;
		}

		//2マス先が壁だったら通路に変更
		if (maze[dir00puls2][dir01puls2] == wall)
		{
			//通路に変更
			maze[dir00puls][dir01puls] = road;
			maze[dir00puls2][dir01puls2] = road;
		}
		else
		{
			continue;
		}

		//注目点を2マス先に
		//再起処理
		Dig(dir00puls2, dir01puls2);
	}
}