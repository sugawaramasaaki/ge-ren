#include "ResultScene.h"
#include "../Engine/Image.h"
#include "../PlayScene/Data.h"

//コンストラクタ
ResultScene::ResultScene(GameObject * parent)
	: GameObject(parent, "ResultScene"),hPict_(-1)
{
}

//初期化
void ResultScene::Initialize()
{
	//画像データのロード
	hPict_ = Image::Load("../Assets/Object/Result_.png");
	assert(hPict_ >= 0);

	resultPoint_ = Data::ReturnPoint();

	
}

//更新
void ResultScene::Update()
{
}

//描画
void ResultScene::Draw()
{
	Image::Draw(hPict_);
}

//開放
void ResultScene::Release()
{
}
