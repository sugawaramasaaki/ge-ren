#pragma once

//■■シーンを管理するクラス
class Gravity
{
	float gravity_;

public:
	Gravity();
	~Gravity();

	void Fall(float speed, float height);
};