#include "Direct2D.h"
#include <wchar.h>
#include "Direct3D.h"
#include "Global.h"
#include "Text.h"

namespace Direct2D
{
    ID2D1Factory* pD2DFactory = nullptr;
    IDWriteFactory* pDWriteFactory = nullptr;
    ID2D1RenderTarget* pRT = nullptr;
    IDXGISurface* pDXGISurface = nullptr;

    UINT createDeviceFlags = 0;
}

HRESULT Direct2D::Initialize()
{

	HRESULT hr;
	// Direct2D,DirectWriteの初期化
	hr = D2D1CreateFactory(D2D1_FACTORY_TYPE_SINGLE_THREADED, &pD2DFactory);
	if (FAILED(hr))
		return hr;

	//予想
	//スワップチェインに、Direct2Dの書き込むバッファ部分をもらう？
	hr = Direct3D::pSwapChain_->GetBuffer(0, IID_PPV_ARGS(&pDXGISurface));
	if (FAILED(hr))
		return hr;

	FLOAT dpiX;
	FLOAT dpiY;
	pD2DFactory->GetDesktopDpi(&dpiX, &dpiY);

	D2D1_RENDER_TARGET_PROPERTIES props =
		D2D1::RenderTargetProperties(D2D1_RENDER_TARGET_TYPE_DEFAULT,
			D2D1::PixelFormat(DXGI_FORMAT_UNKNOWN, D2D1_ALPHA_MODE_PREMULTIPLIED), dpiX, dpiY);


	//Initializeの　Create〜関数にて、　第4引数の　フラグに適した値を入れないと、
	//ここでRenderTargetViewに　ポインタが入らない
	hr = pD2DFactory->CreateDxgiSurfaceRenderTarget(pDXGISurface,
		&props, &pRT);
	if (FAILED(hr))
		return hr;

	hr = DWriteCreateFactory(DWRITE_FACTORY_TYPE_SHARED, __uuidof(IDWriteFactory), reinterpret_cast<IUnknown**>(&pDWriteFactory));
	if (FAILED(hr))
		return hr;

	return S_OK;
}

void Direct2D::Draw()
{
	pRT->BeginDraw();

	Text::Draw();

	pRT->EndDraw();
}

void Direct2D::Release()
{
	Text::Release();
	SAFE_RELEASE(pD2DFactory);
	SAFE_RELEASE(pDWriteFactory);
	SAFE_RELEASE(pRT);
	SAFE_RELEASE(pDXGISurface);
}

UINT Direct2D::Get2dFlags()
{
	createDeviceFlags = D3D11_CREATE_DEVICE_BGRA_SUPPORT;//DirectX11上でDirect2Dを使用するために必要	//この値をCreateDeviceの第4引数に入れなければ、　Direct2Dの描画、　レンダーターゲットビューを取得できない//逆に言えば、　これを第4引数に入れるだけで、　Direct2Dによって、　書き込まれる準備が整う
#ifdef _DEBUG
	createDeviceFlags |= D3D11_CREATE_DEVICE_DEBUG;
#endif

	return createDeviceFlags;
}
