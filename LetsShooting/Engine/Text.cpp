//#include <vector>
//#include "Text.h"
//#include "Global.h"
//
//namespace Text
//{
//    struct TextFormat
//    {
//        IDWriteTextFormat* pTextFomat_;
//        float fontSize_;
//        std::string fontType_;
//    };
//
//    struct TextColor
//    {
//        ID2D1SolidColorBrush* pSolidBrush_;
//        D2D1::ColorF::Enum textColor_;
//    };
//
//    struct TextData
//    {
//        std::wstring text_;
//        Point point_;
//        TextFormat* fomat_;
//        TextColor* color_;
//    };
//
//    std::vector<TextData*> datas_;
//    std::vector<TextFormat*> formats_;
//    std::vector<TextColor*> colors_;
//
//    std::vector<bool> isDraw_;
//
//    std::wstring StringToWString(std::string str)
//    {
//        int bufferSize = MultiByteToWideChar(CP_ACP, 0, str.c_str(), -1, (wchar_t*)NULL, 0);
//
//        wchar_t* cpUCS2 = new wchar_t[bufferSize];
//
//        MultiByteToWideChar(CP_ACP, 0, str.c_str(), -1, cpUCS2, bufferSize);
//
//        std::wstring ret(cpUCS2, cpUCS2 + bufferSize - 1);
//
//        delete[] cpUCS2;
//
//        return(ret);
//    }
//
//    TextFormat* CreateFormat(std::string fontType, float fontSize)
//    {
//        TextFormat* pFormat = nullptr;
//        for (TextFormat* format_ : formats_)
//        {
//            if (format_->fontSize_ == fontSize && format_->fontType_ == fontType) { pFormat = format_; }
//        }
//
//        if (!pFormat)
//        {
//            pFormat = new TextFormat;
//            pFormat->fontSize_ = fontSize;
//            pFormat->fontType_ = fontType;
//
//            Direct2D::pDWriteFactory->CreateTextFormat(
//                std::wstring(fontType.begin(),
//                    fontType.end()).c_str(),
//                nullptr,
//                DWRITE_FONT_WEIGHT_NORMAL,
//                DWRITE_FONT_STYLE_NORMAL,
//                DWRITE_FONT_STRETCH_NORMAL,
//                fontSize,
//                L"",
//                &pFormat->pTextFomat_
//            );
//
//            pFormat->pTextFomat_->SetTextAlignment(DWRITE_TEXT_ALIGNMENT_LEADING);
//        }
//
//        return pFormat;
//    }
//
//    int Text::SetText(std::string textData, std::string fontType, Point point, float fontSize, D2D1::ColorF::Enum color)
//    {
//
//        return 0;
//    }
//
//    void Text::ChangeText(int handle, std::string textData)
//    {
//    }
//
//    void Text::ChangePoint(int handle, Point point)
//    {
//    }
//
//    void Text::ChangeColor(int handle, D2D1::ColorF::Enum color)
//    {
//    }
//
//}