#pragma once

#include <Windows.h>
#include <d2d1.h>
#include <dwrite.h>
#include <string>

#pragma comment(lib,"d2d1.lib")
#pragma comment(lib, "dwrite.lib")

namespace Direct2D
{
	extern IDWriteFactory* pDWriteFactory;
	extern ID2D1RenderTarget* pRT;

	HRESULT Initialize();

	void Draw();

	void Release();

	UINT Get2dFlags();
}