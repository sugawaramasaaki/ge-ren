#include "Wall.h"
#include "../Engine/Direct3D.h"
#include "../Engine/Model.h"

//コンストラクタ
Wall::Wall(GameObject* parent)
	: GameObject(parent, "Wall")
	, hModel_(-1)
{
}

//初期化
void Wall::Initialize()
{
	//モデルデータ読み込み
	hModel_ = Model::Load("../Assets/Object/Wall.fbx");
	assert(hModel_ >= 0);

	//当たり判定
	BoxCollider* collisionLeft = new BoxCollider(XMVectorSet(ps.vecX + 6.f, ps.vecY, ps.vecZ, 0), XMVectorSet(0.1f, 1.f, 1.f, 0));
	AddCollider(collisionLeft);

	BoxCollider* collisionRight = new BoxCollider(XMVectorSet(ps.vecX - 6.f, ps.vecY, ps.vecZ, 0), XMVectorSet(0.1f, 1.f, 1.f, 0));
	AddCollider(collisionRight);

	BoxCollider* collisionBefore = new BoxCollider(XMVectorSet(ps.vecX, ps.vecY, ps.vecZ + 11.f, 0), XMVectorSet(1.f, 1.f, 0.1f, 0));
	AddCollider(collisionBefore);

	BoxCollider* collisionAfter = new BoxCollider(XMVectorSet(ps.vecX, ps.vecY, ps.vecZ - 11.f, 0), XMVectorSet(1.f, 1.f, 0.1f, 0));
	AddCollider(collisionAfter);

}

//更新
void Wall::Update()
{
}

//描画
void Wall::Draw()
{
	Model::SetTransform(hModel_, transform_);
	Model::Draw(hModel_);
}

//開放
void Wall::Release()
{
}
