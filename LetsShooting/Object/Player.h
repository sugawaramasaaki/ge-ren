#pragma once
#include "../Engine/GameObject.h"

//プレイヤーを管理するクラス
class Player : public GameObject
{
	//モデル番号
	int hModel_;

	//初期位置
	float InitialPosition_ = 4.7f;

	//移動速度
	float moveSpeed_;

	//回転角度
	float angle_;

	//左右の移動制限
	float Xlimit_;

	//前後の移動制限
	float Zlimit_;

	//カメラ座標
	//XMVECTOR CameraPos_;

	//現在地とゴールの差分
	XMVECTOR distance = { 0,0,0,0 };

	//ゴールの座標
	XMVECTOR Goal_ = { 0,0,9.5f,0 };

public:
	//コンストラクタ
	//引数：parent  親オブジェクト（SceneManager）
	Player(GameObject* parent);

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//描画
	void Draw() override;

	//開放
	void Release() override;

	//右手のボーン座標を返す
	XMVECTOR GetRightHand();

	//右手のボーン座標を返す
	XMVECTOR GetHead();
};