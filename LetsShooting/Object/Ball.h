#pragma once
#include <tuple>
#include <vector>
#include "../Object/Player.h"

#include "../Engine/GameObject.h"
//前方宣言
class Player;

//シュート番号登録
enum CoasType
{
	LeftRayUp,		//左からのレイアップシュート
	RightRayUp,		//右からのレイアップシュート
	FreeThrow,		//フリースロー
	ThreeShoot,		//3Pシュート
	End,
};

//ボールオブジェクトを管理するクラス
class Ball : public GameObject
{
	//シュートの1フレーム目のみ役割発動
	bool OnTheWay_ = false;

	//
	bool Dribble_ = true;

	//モデル番号
	int hModel_;

	//シュート番号確保用変数
	//99...シュート可能   それ以外...シュート選択中
	int ShootNumber_ = 99;

	//回転角度
	float angle_ = 0;

	//ドリブルの移動速度
	float DribbliSpeed_;

	//ドリブルの下限高さ
	float FloorLimit_;

	//ドリブルの上限高さ
	float HandLimit_;

	float rotate_ = -10.0f;

	float ColliderR_ = 0.075f;

	//重力加速度
	float g_ = 0.00098;

	//各シュート情報格納用変数
	//XMVECTOR：運動ベクトル
	//float：重力の影響による減衰値確保用
	//シュートの運動ベクトル、および重力による減衰値設定
	//左からのレイアップ、右からのレイアップ、フリースロー、3Pポイントシュートの順番に定義
	std::tuple<XMVECTOR, float> type[End]{
		{ XMVectorSet(0.f, 0.f, 0.f, 0.f), 0.0002f},
		{ XMVectorSet(0.f, 0.f, 0.f, 0.f), 0.0002f},
		{ XMVectorSet(0.f, 0.046f, 3.7f, 0.f), 3.7f},
		{ XMVectorSet(0.f, 0.37f, 6.1f, 0.f), 6.1f}
	};

	//シュートコースベクトル格納用
	XMVECTOR coas_ = { 0,0,0,0 };

	//ゴールの座標
	XMVECTOR Goal_ = { 0,0,9.5f,0 };

	XMVECTOR Cameraorigin = { 0,0,9,0 };

	XMVECTOR distance_ = {0,0,0,0};

	//自身の位置とゴールの差分
	XMVECTOR direction_ = { 0,0,0,0 };

	//軌道記録用
	std::vector<float> test;

	//プレイヤーの右腕のボーン座標を取得するため用意
	Player* pPlayer_;

public:
	//コンストラクタ
	//引数：parent  親オブジェクト（SceneManager）
	Ball(GameObject* parent);

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//描画
	void Draw() override;

	//開放
	void Release() override;

	//アニメーション初期化
	void Dribble();

	//シュートを撃つ
	//引数：現在の位置ベクトル
	//引数：どのシュートを撃つか
	XMVECTOR ShootCoas(XMVECTOR before, int shootType);

	//ボールの軌道を取得
	bool GetCoas();

	//当たり判定
	void OnCollision(GameObject* pTaget) override;

	//シュート中か取得
	int GetShootNumber();
};