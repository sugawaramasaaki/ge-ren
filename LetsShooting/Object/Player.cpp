#include "Player.h"
#include "../Engine/Direct3D.h"
#include "../Engine/Model.h"
#include "../Engine/Input.h"
#include "../Engine/Camera.h"
#include "../Engine/SphereCollider.h"
#include "../PlayScene/Data.h"

//コンストラクタ
Player::Player(GameObject * parent)
	: GameObject(parent, "Player"),Xlimit_(6.f),Zlimit_(11.f),angle_(0.0f)
	, hModel_(-1)
	, moveSpeed_(0.05f)
{
}

//初期化
void Player::Initialize()
{
	//モデルデータ読み込み
	hModel_ = Model::Load("../Assets/Object/Chara.fbx");
	assert(hModel_ >= 0);

	//初期位置に移動
	ps.vecZ = InitialPosition_;
}

//更新
void Player::Update()
{
	//------プレイヤー操作------//

	//プレイヤー移動
	//左移動
	if (Input::IsKey(DIK_A))
	{
		ps.vecX -= moveSpeed_;
	}

	//右移動
	if (Input::IsKey(DIK_D))
	{
		ps.vecX += moveSpeed_;
	}

	//後ろ移動
	if (Input::IsKey(DIK_S))
	{
		ps.vecZ -= moveSpeed_;
	}

	//前移動
	if (Input::IsKey(DIK_W))
	{
		ps.vecZ += moveSpeed_;
	}

	//--------------------------//


	//プレイヤーの体の向きをゴールに向ける
	//難易度調整とデバックを楽にする目的

	//現在地とゴール座標の差分
	distance = GetHead() - Goal_;

	//単位ベクトル化
	distance = XMVector3Normalize(distance);


	//回転角度
	angle_ = XMConvertToDegrees
	((XMVector3AngleBetweenVectors
	(g_XMIdentityR2, -distance).vecY));

	angle_ *= XMVector3Normalize(XMVector3Cross
	(g_XMIdentityR2, XMVector3Normalize(-distance))).vecY;

	Camera::SetAngle(angle_);

	//プレイヤーのモデルを回転
	transform_.rotate_.vecY = angle_;

	//ここでベクトル回転
	// 
	// 
	//--------------------

	//ゴールとの距離確保
	Data::SetDirection(distance);

	//行動可能範囲の制限
	//壁を作っているので貫通しないようにする目的

	//左右の移動制限
	if (transform_.position_.vecX > Xlimit_)
	{
		transform_.position_.vecX = Xlimit_;
	}
	if (transform_.position_.vecX < -Xlimit_)
	{
		transform_.position_.vecX = -Xlimit_;
	}

	//前後の移動制限
	if (transform_.position_.vecZ > Zlimit_)
	{
		transform_.position_.vecZ = Zlimit_;
	}
	if (transform_.position_.vecZ < -Zlimit_)
	{
		transform_.position_.vecZ = -Zlimit_;
	}
}

//描画
void Player::Draw()
{
	Model::SetTransform(hModel_, transform_);
	Model::Draw(hModel_);
}

//開放
void Player::Release()
{
}

//右手のボーン座標を返す
XMVECTOR Player::GetRightHand()
{
	//右手のボーンの座標を返す
	return Model::GetBonePosition(hModel_,"Right_Hand");
}

XMVECTOR Player::GetHead()
{
	return Model::GetBonePosition(hModel_,"Head");
}
