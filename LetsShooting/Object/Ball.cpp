#include"Ball.h"
#include "../Engine/Model.h"
#include "../Engine/Input.h"
#include "../Engine/Direct3D.h"
#include "../Engine/Camera.h"
#include "../Engine/SceneManager.h"
#include "../Object/Player.h"
#include "../Object/GoalTexture.h"
#include "../Object/Wall.h"
#include "../PlayScene/Data.h"

//コンストラクタ
Ball::Ball(GameObject * parent)
	: GameObject(parent, "Ball"),hModel_(-1),DribbliSpeed_(0.01f),FloorLimit_(0.04f),HandLimit_(0.34f)
{
	//プレイヤーの右腕のボーン座標を取得するために用意
	pPlayer_ = dynamic_cast<Player*>(FindObject("Player"));
}

//初期化
void Ball::Initialize()
{
	//モデルデータ読み込み読み込み
	hModel_ = Model::Load("../Assets/Object/BasketBall.fbx");
	assert(hModel_ >= 0);

	//ボールにコライダー設置
	SphereCollider* collision = new SphereCollider(ps, ColliderR_);
	AddCollider(collision);

}

//更新
void Ball::Update()
{

	//自分の軌道を記録
	test.push_back(ps.vecY);


	//シュート選択
	//シュートが終了しているときのみ起動
	if (ShootNumber_ == 99)
	{
		//ドリブル
		Dribble();


		//右からのレイアップ実行
		if (Input::IsKeyDown(DIK_LEFT))
		{
			//シュート番号記録
			ShootNumber_ = RightRayUp;

			//フラグオン
			OnTheWay_ = true;
		}


		//左からのレイアップ実行
		if (Input::IsKeyDown(DIK_RIGHT))
		{
			//シュート番号記録
			ShootNumber_ = LeftRayUp;

			//フラグオン
			OnTheWay_ = true;
		}

		//フリースロー実行
		if (Input::IsKeyDown(DIK_UP))
		{
			//シュート番号記録
			ShootNumber_ = FreeThrow;

			//フラグオン
			OnTheWay_ = true;
		}

		//3Pシュート実行
		if (Input::IsKeyDown(DIK_DOWN))
		{
			//シュート番号記録
			ShootNumber_ = ThreeShoot;

			//フラグオン
			OnTheWay_ = true;
		}
		//------------------------
		//カメラをプレイヤーに追従

		direction_ = Data::GetDirection();

		direction_.vecX *= 4.0f;
		direction_.vecZ *= 4.0f;
		direction_.vecY = 2.0f;

		Camera::SetPosition(pPlayer_->GetPosition() + direction_);
		Camera::SetTarget(Cameraorigin);
		// 
		//------------------------

		

	}

	//シュート中処理
	else
	{
		//シュート処理
		//引数：現在の座標
		//引数：シュート番号
		ps = ShootCoas(ps, ShootNumber_);
		transform_.rotate_.vecX += rotate_;

		//----------------------------------------
		// 
		Data::SetDirection(ps - Goal_);

		distance_ = Data::GetDirection();
		distance_.vecX = 0.0f;
		distance_.vecY = 0.5f;
		distance_.vecZ = -0.5f;

		//ゴールに入ったら
		if (ShootNumber_ != 0)
		{
			//カメラがボールを追いかける処理
			Camera::SetPosition(ps + distance_);
			Camera::SetTarget(ps);
		}

		// 
		//----------------------------------------


		
		//シュートした瞬間終了
		OnTheWay_ = false;
	}

	//シュート終了（Y座標がマイナスになった）
	if (ps.vecY < 0.f) 
	{
		//右腕に移動
		ps = pPlayer_->GetRightHand();

		//シュート可能状態に変更
		ShootNumber_ = 99;
	}
}

//描画
void Ball::Draw()
{
	Model::SetTransform(hModel_, transform_);
	Model::Draw(hModel_);
}

//開放
void Ball::Release()
{
}

//ドリブル
void Ball::Dribble()
{
	//現在地の更新
	ps.vecX = pPlayer_->GetRightHand().vecX;
	ps.vecZ = pPlayer_->GetRightHand().vecZ;

	//現在地とゴール座標の差分
	direction_ = ps - Goal_;

	//単位ベクトル化
	direction_ = XMVector3Normalize(direction_);

	//回転角度
	angle_ = XMConvertToDegrees
		((XMVector3AngleBetweenVectors
	(g_XMIdentityR2, -direction_).vecY));

	angle_ *= XMVector3Normalize(XMVector3Cross
	(g_XMIdentityR2, XMVector3Normalize(-direction_))).vecY;

	//ボール回転
	transform_.rotate_.vecY = angle_;

	//ボールを下へドリブル
	if (Dribble_)
	{
		ps.vecY -= DribbliSpeed_;
	}

	//跳弾
	else
	{
		ps.vecY += DribbliSpeed_;
	}

	//ボールが床にぶつかったら
	if (ps.vecY < FloorLimit_ && Dribble_)
		Dribble_ = false;

	//ボールが手から放られたら
	else if (ps.vecY > HandLimit_ && !Dribble_)
		Dribble_ = true;
}


//シュートを撃つ
XMVECTOR Ball::ShootCoas(XMVECTOR before, int shootType)
{	
	//Y座標確保用変数
	static float Y;

	//シュートした瞬間のフレームだったら
	if (OnTheWay_) {

		//シュートの運動ベクトル確保
		coas_ = std::get<0>(type[shootType]);

		//Y座標確保
		Y = coas_.vecY;

		XMMATRIX m = XMMatrixRotationY(XMConvertToRadians(angle_));	//Ｙ軸で30度回転させる行列
		coas_ = XMVector3TransformCoord(coas_, m);	//ベクトルｖを行列ｍで変形

	}

	//ポジション更新
	static float Z = coas_.vecZ / 1.2f / 60;
	before.vecZ += Z;
	before.vecY += Y;

	//Y座標の差分反映
	Y -= 0.0098;

	

	//反映後のベクトルを返す
	return before;
}

//ボールの軌道を返す
bool Ball::GetCoas()
{
	int Count = test.size() - 1;

	float judge = test[Count - 10] - test[Count];

	if (judge > 0) return true;

	else return false;

}

//ゴール判定
void Ball::OnCollision(GameObject* pTaget)
{
	//ボールがぶつかったら
	if (pTaget->GetObjectName() == "GoalJudge")
	{
		//ボールが上から落ちてきている
		if (GetCoas())
		{

			//シュートが入ったので得点加算
			Data::SetPoint(ShootNumber_);

			//ゴールに入った
			if (ShootNumber_ != 0) {
				Instantiate<GoalTexture>(this);
			}

			//複数回処理しても大丈夫なように0に変更
			ShootNumber_ = 0;
		}
	}

	//前後の当たり判定
	if (pTaget->GetObjectName() == "RingBeforeAndAfter")
	{
		coas_.vecZ *= -1;
		coas_.vecY *= -1;
	}

	//左右の当たり判定
	if (pTaget->GetObjectName() == "RingLeftAndRight")
	{
		coas_.vecX *= -1;
		coas_.vecY *= -1;
	}

	//ゴール板との当たり判定
	if (pTaget->GetObjectName() == "Board")
	{
		if (ps.vecY > 2.45f)
		{
			coas_.vecY *= -1;
		}
		else {
			coas_.vecX *= -1;
			coas_.vecZ *= -1;
		}
	}

	//壁
	if (pTaget->GetObjectName() == "Wall")
	{

	}
}

//シュートしているか
int Ball::GetShootNumber()
{
	return ShootNumber_;
}

//XMVECTOR Ball::GetShootCoas()
//{
//	//return coas;
//}
