#include "CourtSide.h"
#include "../Engine/Direct3D.h"
#include "../Engine/Model.h"

//コンストラクタ
CourtSide::CourtSide(GameObject* parent)
	: GameObject(parent, "CourtSide")
	, hModel_(-1)
{
}

//初期化
void CourtSide::Initialize()
{
	//モデルデータ読み込み
	hModel_ = Model::Load("../Assets/Object/CourtSide.fbx");
	assert(hModel_ >= 0);

}

//更新
void CourtSide::Update()
{
}

//描画
void CourtSide::Draw()
{
	Model::SetTransform(hModel_, transform_);
	Model::Draw(hModel_);
}

//開放
void CourtSide::Release()
{
}
