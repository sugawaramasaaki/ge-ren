#pragma once
#include "../Engine/GameObject.h"

//タイトルシーンを管理するクラス
class GoalTexture : public GameObject
{
	int hPict_;			//画像番号

	float Increase_ = 0.01f;

	float MaxSize_ = 1.5f;

	float SponeScale_ = 0.5f;
public:
	//コンストラクタ
	//引数：parent  親オブジェクト（SceneManager）
	GoalTexture(GameObject* parent);

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//描画
	void Draw() override;

	//開放
	void Release() override;
};