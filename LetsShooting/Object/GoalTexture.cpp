#include "GoalTexture.h"
#include "../Engine/Image.h"
#include "../Engine/Input.h"
#include "../Engine/SceneManager.h"


//コンストラクタ
GoalTexture::GoalTexture(GameObject* parent)
	: GameObject(parent, "GoalTexture"), hPict_(-1)
{
}

//初期化
void GoalTexture::Initialize()
{
	//画像データのロード
	hPict_ = Image::Load("../Assets/Object/Goal.png");
	assert(hPict_ >= 0);

	transform_.scale_ *= SponeScale_;
}

//更新
void GoalTexture::Update()
{
	//画像サイズ変更
	if (transform_.scale_.vecX < MaxSize_) {
		transform_.scale_.vecX += Increase_;
		transform_.scale_.vecY += Increase_;
		transform_.scale_.vecZ += Increase_;
	}
	else {
		KillMe();
	}
}

//描画
void GoalTexture::Draw()
{
	Image::SetTransform(hPict_, transform_);
	Image::Draw(hPict_);
}

//開放
void GoalTexture::Release()
{
}
