#include "TimerImg.h"
#include "../Engine/Image.h"
#include "../Engine/Input.h"
#include "../Engine/SceneManager.h"


//コンストラクタ
TimerImg::TimerImg(GameObject* parent)
	: GameObject(parent, "StartButton"), hPict_(-1)
{
}

//初期化
void TimerImg::Initialize()
{
	//画像データのロード
	hPict_ = Image::Load("../Assets/Object/timerback.png");
	assert(hPict_ >= 0);

	transform_.position_.vecX = ButtonXpos_;
	transform_.position_.vecY = ButtonYpos_;


}

//更新
void TimerImg::Update()
{
}

//描画
void TimerImg::Draw()
{
	Image::SetTransform(hPict_, transform_);
	Image::Draw(hPict_);
}

//開放
void TimerImg::Release()
{
}

