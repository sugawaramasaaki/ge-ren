#pragma once
#include "../Engine/Direct3D.h"


#define NULLBUTTON 0
#define STARTBUTTON 1
#define HELPBUTTON 2

namespace Data
{
	void SetPoint(int point);

	int ReturnPoint();

	void SetNowSelectButton(int SelectButton);

	int ReturnNowSelectButton();

	void SetDirection(XMVECTOR direction);

	XMVECTOR GetDirection();

	void SetLimitTime(float lt);

	float GetLimitTime();
}