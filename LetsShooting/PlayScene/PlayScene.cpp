#include "PlayScene.h"
#include "Data.h"
#include "TimerImg.h"
#include "TimerNeedle.h"
#include "../ReturnButton/ReturnButton.h"
#include "../Object/Player.h"
#include "../Object/Stage.h"
#include "../Object/Ball.h"
#include "../Object/Wall.h"
#include "../Object/CourtSide.h"
#include "../Goal/GoalObject.h"
#include "../Engine/SceneTimer.h"
#include "../Engine/SceneManager.h"
#include "../TitleScene/StartButton.h"


//コンストラクタ
PlayScene::PlayScene(GameObject * parent)
	: GameObject(parent, "PlayScene")
{
	Data::SetLimitTime(30.f);
}

//初期化
void PlayScene::Initialize()
{
	//マップオブジェクト生成
	Instantiate<Stage>(this);

	//プレイヤー生成
	Instantiate<Player>(this);
	
	//バスケットゴールオブジェクト生成
	Instantiate<GoalObject>(this);

	//コートの通り道生成
	Instantiate<CourtSide>(this);

	//壁生成
	Instantiate<Wall>(this);
	
	//ボール生成
	Instantiate<Ball>(this);

	//戻るボタン
	Instantiate<ReturnButton>(this);

	//タイムアタック
	if (Data::ReturnNowSelectButton() == HELPBUTTON) {
		//タイマー
		Instantiate<TimerImg>(this);

		//秒針
		Instantiate<TimerNeedle>(this);
	}
}

//更新
void PlayScene::Update()
{
	//シーン開始時間確保
	//タイムアタックをするために時間計測

	if (Data::ReturnNowSelectButton() == HELPBUTTON)
	{
		if (isFirst_)
		{
			timerStart_ = SceneTimer::GetElapsedSecounds();
			isFirst_ = false;
		}

		else
		{
			if (SceneTimer::GetElapsedSecounds() - timerStart_ > Data::GetLimitTime()
				|| Data::ReturnPoint() > limitPoint_)
			{
				dynamic_cast<SceneManager*>(GetParent())->ChangeScene(SCENE_ID_RESULT);
			}
		}
	}
	
}

//描画
void PlayScene::Draw()
{
}

//開放
void PlayScene::Release()
{
}
