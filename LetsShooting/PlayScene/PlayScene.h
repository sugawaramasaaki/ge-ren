#pragma once
#include "../Engine/GameObject.h"


//Playシーンを管理するクラス
class PlayScene : public GameObject
{
private:
	int limitPoint_ = 10;
	float timerStart_ = 0.f;
	bool isFirst_ = true;
public:
	//コンストラクタ
	//引数：parent  親オブジェクト（SceneManager）
	PlayScene(GameObject* parent);

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//描画
	void Draw() override;

	//開放
	void Release() override;

	float GetLinitTime();
};