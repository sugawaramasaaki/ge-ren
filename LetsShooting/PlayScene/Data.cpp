#include "Data.h"

//共有データバンク
namespace Data
{
	//合計獲得点数
	int point_ = 0;

	//初期状態は未選択なので0
	int NowSelectButton_ = NULLBUTTON;

	//
	XMVECTOR direction_ = { 0,0,0,0 };

	float LimitTime_ = 0.f;

}

void Data::SetPoint(int point)
{
	point_ += point;
}

int Data::ReturnPoint()
{
	return point_;
}

void Data::SetNowSelectButton(int SelectButton)
{
	NowSelectButton_ = SelectButton;
}

int Data::ReturnNowSelectButton()
{
	return NowSelectButton_;
}

void Data::SetDirection(XMVECTOR direction)
{
	direction_ = direction;
}

XMVECTOR Data::GetDirection()
{
	return direction_;
}

void Data::SetLimitTime(float lt)
{
	LimitTime_ = lt;
}

float Data::GetLimitTime()
{
	return LimitTime_;
}
