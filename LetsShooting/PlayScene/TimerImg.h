#pragma once
#include "../Engine/GameObject.h"

//タイトルシーンを管理するクラス
class TimerImg : public GameObject
{

	int hPict_;			//画像番号


	float ButtonXpos_ = 0.85f;
	float ButtonYpos_ = 0.8f;

public:
	//コンストラクタ
	//引数：parent  親オブジェクト（SceneManager）
	TimerImg(GameObject* parent);

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//描画
	void Draw() override;

	//開放
	void Release() override;

};