#include "TimerNeedle.h"
#include "Data.h"
#include "../Engine/Image.h"
#include "../Engine/Input.h"
#include "../Engine/SceneManager.h"


//コンストラクタ
TimerNeedle::TimerNeedle(GameObject* parent)
	: GameObject(parent, "StartButton"), hPict_(-1)
{
}

//初期化
void TimerNeedle::Initialize()
{
	//画像データのロード
	hPict_ = Image::Load("../Assets/Object/timerpick.png");
	assert(hPict_ >= 0);

	transform_.position_.vecX = ButtonXpos_;
	transform_.position_.vecY = ButtonYpos_;


}

//更新
void TimerNeedle::Update()
{
	static float rotate = 360.f / Data::GetLimitTime() / 60.f;

	transform_.rotate_.vecZ -= rotate;

	
}

//描画
void TimerNeedle::Draw()
{
	Image::SetTransform(hPict_, transform_);
	Image::Draw(hPict_);
}

//開放
void TimerNeedle::Release()
{
}

