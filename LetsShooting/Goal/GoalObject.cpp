#include "GoalObject.h"
#include "GoalJudge.h"
#include "RingLeftAndRight.h"
#include "RingBeforeAndAfter.h"
#include "Board.h"
#include "../Engine/Direct3D.h"
#include "../Engine/Model.h"


//コンストラクタ
GoalObject::GoalObject(GameObject * parent)
	: GameObject(parent, "GoalObject")
	, hModel_(-1)
{
}

//初期化
void GoalObject::Initialize()
{
	//モデルデータ読み込み
	hModel_ = Model::Load("../Assets/Object/Goal_Object.fbx");
	assert(hModel_ >= 0);

	//ゴールリングの位置に合わせる
	transform_.position_.vecZ = GoalPs_;

	//ゴール判定
	Instantiate<GoalJudge>(this);
	
	//リングの当たり判定
	Instantiate<RingLeftAndRight>(this);
	Instantiate<RingBeforeAndAfter>(this);

	//ゴールボードの当たり判定
	Instantiate<Board>(this);


}

//更新
void GoalObject::Update()
{
}

//描画
void GoalObject::Draw()
{
	Model::SetTransform(hModel_, transform_);
	Model::Draw(hModel_);
}

//開放
void GoalObject::Release()
{
}
