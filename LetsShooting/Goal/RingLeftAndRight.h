#pragma once
#include "../Engine/GameObject.h"

//ゴールリングの右側当たり判定を管理するクラス
class RingLeftAndRight : public GameObject
{
public:
	//コンストラクタ
	//引数：parent  親オブジェクト（SceneManager）
	RingLeftAndRight(GameObject* parent);

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//描画
	void Draw() override;

	//開放
	void Release() override;
};