#include "GoalJudge.h"
#include "../Engine/Direct3D.h"
//コンストラクタ
GoalJudge::GoalJudge(GameObject * parent)
	: GameObject(parent, "GoalJudge")
	, hModel_(-1)
{
}

//初期化
void GoalJudge::Initialize()
{
	//ゴール判定
	BoxCollider* collision = new BoxCollider(XMVectorSet(ps.vecX,ps.vecY + 1.4f,ps.vecZ,0), XMVectorSet(0.02f, 0.06f, 0.02f, 0));
	AddCollider(collision);
}

//更新
void GoalJudge::Update()
{
}

//描画
void GoalJudge::Draw()
{

}

//開放
void GoalJudge::Release()
{
}
