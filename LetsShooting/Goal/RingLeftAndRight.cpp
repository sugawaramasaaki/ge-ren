#include "RingLeftAndRight.h"
#include "../Engine/Direct3D.h"

//コンストラクタ
RingLeftAndRight::RingLeftAndRight(GameObject* parent)
	: GameObject(parent, "RingLeftAndRight")
{
}

//初期化
void RingLeftAndRight::Initialize()
{
	//当たり判定
	BoxCollider* collision1 = new BoxCollider(XMVectorSet(ps.vecX + 0.2f, ps.vecY + 1.8f, ps.vecZ, 0), XMVectorSet(0.02f, 0.02f, 0.5f, 0));
	AddCollider(collision1);

	BoxCollider* collision2 = new BoxCollider(XMVectorSet(ps.vecX - 0.2f, ps.vecY + 1.8f, ps.vecZ, 0), XMVectorSet(0.02f, 0.02f, 0.5f, 0));
	AddCollider(collision2);
}

//更新
void RingLeftAndRight::Update()
{
}

//描画
void RingLeftAndRight::Draw()
{

}

//開放
void RingLeftAndRight::Release()
{
}
