#include "Board.h"
#include "../Engine/Direct3D.h"

//コンストラクタ
Board::Board(GameObject* parent)
	: GameObject(parent, "Board")
{
}

//初期化
void Board::Initialize()
{
	//当たり判定
	BoxCollider* collision = new BoxCollider(XMVectorSet(ps.vecX, ps.vecY + 2.1f, ps.vecZ + 0.35f, 0), XMVectorSet(1.4f, 0.9f, 0.1f, 0));
	AddCollider(collision);
}

//更新
void Board::Update()
{
}

//描画
void Board::Draw()
{

}

//開放
void Board::Release()
{
}
