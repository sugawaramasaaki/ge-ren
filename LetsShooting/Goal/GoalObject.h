#pragma once
#include "../Engine/GameObject.h"

//バスケットゴールオブジェクトを管理するクラス
class GoalObject : public GameObject
{
	int hModel_;		//モデル番号

	float GoalPs_ = 9.5f;
public:
	//コンストラクタ
	//引数：parent  親オブジェクト（SceneManager）
	GoalObject(GameObject* parent);

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//描画
	void Draw() override;

	//開放
	void Release() override;
};