#include "RingBeforeAndAfter.h"
#include "../Engine/Direct3D.h"

//コンストラクタ
RingBeforeAndAfter::RingBeforeAndAfter(GameObject* parent)
	: GameObject(parent, "RingBeforeAndAfter")
{
}

//初期化
void RingBeforeAndAfter::Initialize()
{
	//当たり判定
	BoxCollider* collision1 = new BoxCollider(XMVectorSet(ps.vecX, ps.vecY + 1.8f, ps.vecZ + 0.2f, 0), XMVectorSet(0.5f, 0.02f, 0.02f, 0));
	AddCollider(collision1);

	BoxCollider* collision2 = new BoxCollider(XMVectorSet(ps.vecX, ps.vecY + 1.8f, ps.vecZ - 0.2f, 0), XMVectorSet(0.5f, 0.02f, 0.02f, 0));
	AddCollider(collision2);
}

//更新
void RingBeforeAndAfter::Update()
{
}

//描画
void RingBeforeAndAfter::Draw()
{

}

//開放
void RingBeforeAndAfter::Release()
{
}
