#include "ReturnButton.h"
#include "../Engine/Image.h"
#include "../Engine/Input.h"
#include "../Engine/SceneManager.h"


//コンストラクタ
ReturnButton::ReturnButton(GameObject* parent)
	: GameObject(parent, "StartButton"), hPict_(-1)
{
}

//初期化
void ReturnButton::Initialize()
{
	//画像データのロード
	hPict_ = Image::Load("../Assets/Object/Return04test.png");
	assert(hPict_ >= 0);

	transform_.position_.vecX = ButtonXpos_;
	transform_.position_.vecY = ButtonYpos_;

}

//更新
void ReturnButton::Update()
{
	if (Input::IsKeyDown(DIK_ESCAPE))
	{
		dynamic_cast<SceneManager*>(GetParent()->GetParent())->ChangeScene(SCENE_ID_TITLE);
	}
}

//描画
void ReturnButton::Draw()
{
	Image::SetTransform(hPict_, transform_);
	Image::Draw(hPict_);
}

//開放
void ReturnButton::Release()
{
}

