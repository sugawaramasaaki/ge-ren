#include "HelpButton.h"
#include "../Engine/Image.h"
#include "../Engine/Input.h"
#include "../PlayScene/Data.h"
#include "../Engine/SceneManager.h"


//コンストラクタ
HelpButton::HelpButton(GameObject* parent)
	: GameObject(parent, "HelpButton"),hPict_(-1), pictSizevecX_(0.015f), pictSizevecY_(0.01f), upperlimitSize_(1.4f), lowerlimitSize_(0.8f)
{
}

//初期化
void HelpButton::Initialize()
{
	//画像データのロード
	hPict_ = Image::Load("../Assets/Object/T.ATTACK.png");
	assert(hPict_ >= 0);

	transform_.position_.vecX = ButtonXpos_;
	transform_.position_.vecY = ButtonYpos_;
}

//更新
void HelpButton::Update()
{
	//ボタンの処理
	Selecting();
}

//描画
void HelpButton::Draw()
{
	Image::SetTransform(hPict_, transform_);
	Image::Draw(hPict_);
}

//開放
void HelpButton::Release()
{
}

//ボタンの機能
void HelpButton::Selecting()
{
	//現在選択されているなら、視覚的にわかりやすいように表示サイズを変える
	if (Data::ReturnNowSelectButton() == HELPBUTTON)
	{
		//現在の画像サイズに合わせて次どうするかの設定
		switch (flg)
		{
		case defalt: flg = upper;
			break;
		case upper: if (transform_.scale_.vecX > upperlimitSize_) flg = lower;
			break;
		case lower: if (transform_.scale_.vecX < lowerlimitSize_) flg = upper;
			break;

		}

		//だんだん大きく
		if (flg == upper)
		{
			transform_.scale_.vecX += pictSizevecX_;
			transform_.scale_.vecY += pictSizevecY_;
		}
		//だんだん小さく
		else if (flg == lower) {
			transform_.scale_.vecX -= pictSizevecX_;
			transform_.scale_.vecY -= pictSizevecY_;
		}


		//スタートボタンを選択している かつ 状態遷移を望んだら
		//プレイシーンへ
		if (Input::IsKeyDown(DIK_RETURN))
		{
			dynamic_cast<SceneManager*>(GetParent()->GetParent())->ChangeScene(SCENE_ID_PLAY);
		}
	}

	//選択されていないのでデフォルトの大きさに戻す
	else
	{
		transform_.scale_.vecX = 1.0f;
		transform_.scale_.vecY = 1.0f;

		flg = defalt;
	}
}
