#include "TitleBase.h"
#include "TitleScene.h"
#include "StartButton.h"
#include "HelpButton.h"
#include "TitleText.h"
#include "../Engine/Image.h"
#include "../Engine/Input.h"
#include "../PlayScene/Data.h"

//コンストラクタ
TitleScene::TitleScene(GameObject * parent)
	: GameObject(parent, "TitleScene")
{
}

//初期化
void TitleScene::Initialize()
{
	//タイトル背景
	Instantiate<TitleBase>(this);

	//スタートボタン配置
	Instantiate<StartButton>(this);
	
	//ヘルプボタン配置
	Instantiate<HelpButton>(this);
	
	//タイトルテキスト配置
	Instantiate<TitleText>(this);
}

//更新
void TitleScene::Update()
{
	//選択中のボタンを上下矢印キーで変更
	if (Input::IsKeyDown(DIK_UP) || Input::IsKeyDown(DIK_DOWN))
	{
		switch (Data::ReturnNowSelectButton())
		{
		case STARTBUTTON: Data::SetNowSelectButton(HELPBUTTON);
			break;
		case HELPBUTTON: Data::SetNowSelectButton(STARTBUTTON);
			break;
		case NULLBUTTON: Data::SetNowSelectButton(STARTBUTTON);
			break;
		}
	}
}

//描画
void TitleScene::Draw()
{
}

//開放
void TitleScene::Release()
{
}
