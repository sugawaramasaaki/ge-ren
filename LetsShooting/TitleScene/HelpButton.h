#pragma once
#include "../Engine/GameObject.h"

//タイトルシーンを管理するクラス
class HelpButton : public GameObject
{
	//大きくするか小さくするか判断するための判定
	char flg = defalt;

	int hPict_;			//画像番号

	float upperlimitSize_;		//サイズ調整時の上限のおおきさ
	float lowerlimitSize_;		//サイズ調整時の下限のおおきさ
	float pictSizevecX_;	//サイズ調整用変数
	float pictSizevecY_;	//サイズ調整用変数

	float ButtonXpos_ = -0.5f;
	float ButtonYpos_ = -0.4f;

public:
	//コンストラクタ
	//引数：parent  親オブジェクト（SceneManager）
	HelpButton(GameObject* parent);

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//描画
	void Draw() override;

	//開放
	void Release() override;

	//選択中
	void Selecting();
};