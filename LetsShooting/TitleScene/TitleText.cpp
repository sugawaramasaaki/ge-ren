#include "TitleText.h"
#include "../Engine/Image.h"
#include "../Engine/Input.h"
#include "../Engine/SceneManager.h"


//コンストラクタ
TitleText::TitleText(GameObject* parent)
	: GameObject(parent, "TitleText"),hPict_(-1)
{
}

//初期化
void TitleText::Initialize()
{
	//画像データのロード
	hPict_ = Image::Load("../Assets/Object/Title.png");
	assert(hPict_ >= 0);
}

//更新
void TitleText::Update()
{
}

//描画
void TitleText::Draw()
{
	Image::SetTransform(hPict_, transform_);
	Image::Draw(hPict_);
}

//開放
void TitleText::Release()
{
}
