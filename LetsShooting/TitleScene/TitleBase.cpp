#include "TitleBase.h"
#include "../Engine/Image.h"
#include "../Engine/Input.h"
#include "../Engine/SceneManager.h"


//コンストラクタ
TitleBase::TitleBase(GameObject* parent)
	: GameObject(parent, "TitleBase"),hPict_(-1)
{
}

//初期化
void TitleBase::Initialize()
{
	//画像データのロード
	hPict_ = Image::Load("../Assets/Object/TitleBase.png");
	assert(hPict_ >= 0);
}

//更新
void TitleBase::Update()
{
}

//描画
void TitleBase::Draw()
{
	Image::SetTransform(hPict_, transform_);
	Image::Draw(hPict_);
}

//開放
void TitleBase::Release()
{
}
