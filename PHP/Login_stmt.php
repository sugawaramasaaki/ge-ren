<?php
	// 簡易ログインシステム
	define( "DBHOST", "127.0.0.1" );
	define( "DBNAME", "ge3a_db" );
	define( "DBUSER", "ge3a" );
	define( "DBPASS", "ge3a" );
	
	$mysqli = new mysqli( DBHOST, DBUSER, DBPASS, DBNAME );

	// アカウント情報入力
	echo "account:";
	$user_account = trim( fgets(STDIN) );
	
	echo "password:";
	$user_password = trim( fgets(STDIN) );
	
	// プレースホルダ入りのSQL生成
	$sql = "SELECT COUNT(*) FROM user_tbl WHERE user_account = ? AND user_password = PASSWORD( ? )";
	echo $sql . "\n";	// 確認用
	
	// SQLセット
	$stmt = $mysqli->prepare( $sql );
	
	// プレースホルダに値セット
	$stmt->bind_param( "ss", $user_account, $user_password );
	
	// 実行
	$stmt->execute();
	
	// 結果取得用の変数準備
	$stmt->bind_result( $count );
	
	if( $stmt->fetch() && $count == 1 ) {
		echo "Success!";
	}
	else{
		echo "Login Error!";
	}
	$mysqli->close();
?>