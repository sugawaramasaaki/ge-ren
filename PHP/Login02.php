<?php
	define("DSN","mysql:dbname=ge3a_db;host=127.0.0.1");
	define("UN","ge3a");
	define("PS","ge3a");
	
	try{
		//コネクト
		$pdo = new PDO(DSN,UN,PS);
		
		echo "アカウント名を入力してください\n";
		echo "account    :";
		
		$account = trim(fgets(STDIN));
		
		echo "パスワードを入力してください\n";
		echo "password   :";
		
		$password = trim(fgets(STDIN));
		
		$sql = "SELECT * FROM user_tbl WHERE user_account = :account && 
									user_password = PASSWORD(:password)";
		
		$stmt = $pdo->prepare( $sql );
		
		$stmt->bindParam(":account",$account);
		$stmt->bindParam(":password",$password);
		
		$stmt->execute();
		if($stmt->fetch()){
			echo "Success!";
		}
		else{
			echo "Login Error!";
		}
	}
	catch(PDOException $ex){
		die("Error:" . $ex->getMessage());
	}
	
	$pdo = null;
?>