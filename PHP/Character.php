<?php
	//デフォルトのキャラクターのステータス
	define("defaultHP",100);
	define("defaultMP",30);
	
	//キャラクター親クラス
	class Character{
		
		//キャラクターのHP
		private $hp;
		//キャラクターのMP
		private $mp;
		
		//一番最近受けたダメージ
		private $recentlyDamage = 0;
		
		//現在の被ダメカットバフの効果値	※○○%　例）30%カット
		private $damagecutvalue = 0;
		
		
		//コンストラクタ
		//引数：設定するHP、MP
		function __construct($hp_ = defaultHP, $mp_ = defaultMP){
			//echo "construct\n";
			$this->hp = $hp_;
			$this->mp = $mp_;
		}
		
		//デストラクタ
		function __destruct(){
			//echo "\ndestruct";
		}
		
		//-------------------セッター・ゲッター-------------------//
		
		//現在のHPを返す
		public function gethp(){ return $this->hp; }
		
		//HPを設定する
		//引数：変更後のHP
		public function sethp($afterHp_){ $this->hp = $afterHp_; }
		
		//現在のMPを返す
		public function getmp(){ return $this->mp; }
		
		//MPを設定する
		//引数：変更後のMP
		public function setmp($afterMp_){ $this->mp = $afterMp_; }
		
		//最近受けたダメージを返す
		public function getrecentlyDamage(){ return $this->recentlyDamage; }
		
		//一番最近受けたダメージ
		//引数：ダメージ値
		public function setrecentlyDamage($damage_){ $this->recentlyDamage = $damage_; }
		
		//ダメージカット効果値を返す
		public function getdamagecutvalue(){ return $this->damagecutvalue; }
		
		//ダメージカット効果値設定
		//引数：最近受けたダメージ値
		public function setdamagecutvalue($damagecutbuff_){ $this->damagecutvalue = $damagecutbuff_; }
		
		//--------------------------------------------------------//
		
		//被ダメ軽減発動
		//引数：ダメージ値
		public function damagecut($damage_){
			return $damage_ * ((100 - $this->getdamagecutvalue()) * 0.01);
		}
		
		//ダメージ受けたてきな
		//引数：ダメージ値
		public function hurt(){
			$this->hp -= $this->damagecut($this->getrecentlyDamage());
		}
	}
?>