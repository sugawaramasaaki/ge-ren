<!doctype html>
	<html>
	<head>
		<meta charset="UTF-8">
		<title>商品検索ページ</title>
		<script type="text/javascript">
		//入力した値段のチェック
		function check(){
			if(document.getElementById("lower").value == ""){
				document.getElementById("lower").value = 0;
			}
			if(document.getElementById("upper").value == ""){
				document.getElementById("upper").value = 99999;
			}
			if(parseInt(document.getElementById("lower").value) > parseInt(document.getElementById("upper").value)){
				alert("価格の設定に誤りがあります");
				return false;
			}else{
				return true;
			}
		}
		</script>
	</head>
	<body>
		<h1>商品検索ページ</h1>
		<form action="./SearchProductsResult.php" method="POST">
			<p>検索キーワード：<input type="text" id="word" name="word" placeholder="キーワード">※キーワードが複数の場合はスペースで区切ってください</p>
			<p>値段範囲　　　：<input type="number" id="lower" name="lower" placeholder="0">円～<input type="number" id="upper" name="upper" placeholder="99999">円
			<select name="sort">
				<option value="NameASC" selected>商品名昇順</option>
				<option value="NameDESC">商品名降順</option>
				<option value="CodeASC">商品コード昇順</option>
				<option value="CodeDESC">商品コード降順</option>
				<option value="PriceASC">価格昇順</option>
				<option value="PriceDESC">価格降順</option>
				<option value="MemoASC">備考昇順</option>
				<option value="MemoDESC">備考降順</option>
			</select>
			<input type="submit" value="検索" onclick="return check()"></p>
		</form>
	</body>
	</html>