<!doctype html>
<html>
	<head>
		<meta charset="UTF-8">
		<title>商品検索ページ</title>

		<script type="text/javascript">
			function inputClear(){
				input.key.value = "";
			}
			function inputCheck(){
				if(input.min.value >= 0 && input.max.value == "")return true;
				if(input.min.value == "" && input.max.value >= 0)return true;
				if(input.min.value > input.max.value || 
				input.min.value < 0 || input.max.value < 0){
					alert("価格の設定に誤りがあります");
					return false;
				}else{
					return true;
				}
			}
		</script>
	</head>
	<body>
		<h1>商品検索ページ</h1>
		<form action="SearchProductsResult.php" method="post" name="input">
			<p>キーワードの入力:<input type="text" name="word">&nbsp;<button type="button" onclick="inputClear()">クリア</button></p>
			<p>価格:<input type="number" name="lower" style="width:100px">～<input type="number" name="upper" style="width:100px">
			<input type="submit" value="🔍検索" onclick="return inputCheck()"></p>
		</form>
	</body>
</html>