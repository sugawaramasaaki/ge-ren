<?php
	// 簡易ログインシステム
	define( "DBHOST", "127.0.0.1" );
	define( "DBNAME", "ge3a_db" );
	define( "DBUSER", "ge3a" );
	define( "DBPASS", "ge3a" );
	
	//DBにアクセス
	$mysqli = new mysqli( DBHOST, DBUSER, DBPASS, DBNAME );
	if($mysqli->connect_error)
	{
		die("Login Error!");
	}
	
	echo "アカウント名を入力してください\n";
	echo "account    :";
	$user_account = trim(fgets(STDIN));
	
	echo "パスワードを入力してください\n";
	echo "password   :";
	$user_password = trim(fgets(STDIN));
	
	//SQL
	$sql = "INSERT INTO user_tbl( user_account, user_password) VALUES( ?, PASSWORD( ? ))";
	$stmt = $mysqli->prepare($sql);
	
	//プレースホルダを組み込んだSQLを準備
	$stmt->bind_param("ss",$user_account,$user_password);
	
	//実行
	$stmt->execute();
	
	//アカウントが追加されたかの確認
	$sqlcheck = "select count(*) from user_tbl where user_account = ? && user_password = PASSWORD( ? )";
	$stmtcheck = $mysqli->prepare($sqlcheck);
	
	$stmtcheck->bind_param("ss",$user_account,$user_password);
	
	$stmtcheck->execute();
	
	// 結果取得用の変数準備
	$stmtcheck->bind_result( $count );
	
	if( $stmtcheck->fetch() && $count == 1 ) {
		echo "Success!";
	}
	else{
		echo "Login Error!";
	}
	$mysqli->close();
?>