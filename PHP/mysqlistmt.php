<?php
	// 簡易ログインシステム
	define( "DBHOST", "127.0.0.1" );
	define( "DBNAME", "ge3a_db" );
	define( "DBUSER", "ge3a" );
	define( "DBPASS", "ge3a" );
	
	$mysqli = new mysqli( DBHOST, DBUSER, DBPASS, DBNAME );
	
	//プリペアドステートメント　?がプレースホルダ
	$prepare = "select + from user_tbl whwre user_account = ? and user_password = PASSWORD(?)";
	
	//プリペアードステートメントを用意？？　戻り値mysqli_stmtクラス
	$stmt = $mysqli->prepare($prepare);
	
	// アカウント情報入力
	echo "account:";
	$user_account = trim( fgets(STDIN) );
	
	echo "password:";
	$user_password = trim( fgets(STDIN) );
	
	//バインド
	$stmt->bind_param("ss", $user_account,$user_password);
	
	//実行
	$stmt->execute();
	
	$stmt->bind_result($id,$account,$pass,$date);
	
	while($stmt->fetch()){
		echo $id . $account . $pass . $date;
	}
?>