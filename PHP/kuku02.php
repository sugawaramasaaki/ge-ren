<!docktype html>
	<html>
	<link rel="stylesheet" type="text/css" href="./kuku02.css">
	<meta charset="utf-8">
	<head><title>九九表示</title></head>
	<body>
	<table>
	<?php
	for($i = 1; $i < 10; $i++){
		for($j = 1; $j < 10; $j++){
			if($j == 1){
				echo "<tr>";
			}
			echo "<td align=\"right\">" . $i * $j . "</td>";
			if($j == 9){
				echo "</tr>";
			}
		}
	}
	?>
	</table>
	</body>
	</html>