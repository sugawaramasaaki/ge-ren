<?php
	//Enemy
	//Characterクラス継承
	//メンバ変数追加なし
	//メンバ関数attack()実装
	//ランダムで1~100の値を出して返す
	
	require_once("Character.php");
	
	class Enemy extends Character {
		
		//ダメージを返す
		public function attack(){
			return mt_rand(1,100);
		}
	}
?>