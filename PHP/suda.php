<!doctype html>
	<html>
	<head>
		<meta charset="UTF-8">
		<meta http-equiv="refresh" content="300">
		<title>Monitor.php</title>
	</head>
	<body>
		<?php			
			//タイムゾーンを設定
			date_default_timezone_set('Asia/Tokyo');
			//フォルダ内検索(monitor)
			$dirArray = glob("./monitor/*.txt");
			//ファイル件数
			$fileCount = 0;
			//フォルダ内の.txt表示
			foreach($dirArray as $filePath){
				//ファイル件数カウント
				$fileCount++;
				//タイムスタンプ
				$date = date( "Y/m/d", filemtime($filePath));
				//ファイルの中身を開く
				$ofile = fopen($filePath, "r");
				$file = file_get_contents($filePath);
				//ファイル削除
				//unlink($filePath);
				//javascript(新しいタブで開く)
				{
					//<script type=\"text/javascript\">
					echo "<script type=\"text/javascript\">";
					echo "let win" . $fileCount . " = window.open();";
					echo "win" . $fileCount . " .document.open();";
						//<HTML>
						echo "win" . $fileCount . " .document.write(\"<HTML>\");";
							//<HEAD><TITLE>新しいタブ</TITLE></HEAD>
							echo "win" . $fileCount . " .document.write(\"<HEAD><TITLE>新しいタブ</TITLE></HEAD>\");";
							//<BODY>
							echo "win" . $fileCount . " .document.write(\"<BODY>\");";
								//<H1>タイムスタンプ</H1>
								echo "win" . $fileCount . " .document.write(\"<H1>" . $date . "</H1>\");";
								//<H2>ファイルネーム</H2>
								echo "win" . $fileCount . " .document.write(\"<H2>" . $filePath . "</H2>\");";
								//<H3>ファイルの中身</H3>
								//while($line = fgets($ofile)){
								//	echo "win" . $fileCount . " .document.write(\"<H3>" . $line . "</H3><br>\");";
								//}
								echo "win" . $fileCount . " .document.write(\"<H3>" . $file . "</H3>\");";
							echo "win" . $fileCount . " .document.write(\"</BODY>\");";
							//</BODY>
						echo "win" . $fileCount . " .document.write(\"</HTML>\");";
						//<HTML>
					echo "win" . $fileCount . " .document.close();";
					echo "</script>";
					//</script>
				}
				//ファイルを閉じる
				fclose($ofile);
				//ファイル削除
				//unlink($filePath);
			}
			//消したファイル数
			echo date("Y/m/d H:i:s") . "に " . $fileCount . " 件のファイルを削除しました。";
		?>
	</body>
</html>