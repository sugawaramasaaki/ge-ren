<!doctype html>
	<html>
	<head>
	<meta charset="UTF-8">
	<meta http-equiv="refresh" content="300">
	<title>POST受信</title>
	</head>
	<body>
	<?php
	
		//タイムゾーン指定
		date_default_timezone_set('Asia/Tokyo');
		
		//ファイル指定
		$dir = "./monitor/";
		$name[] = "";
		
		$winCnt = 0;
		
		//ファイル探査
		foreach(glob($dir . "{*.txt,.txt}", GLOB_BRACE) as $file){
			//ヒット数増加
			$winCnt++;
			
			//---------出力データ準備---------//
			//ファイル名確保
			$name[$winCnt] = basename($file);
			
			//ファイルのタイムスタンプ確保
			$timeStanp = date("Y-m-d H:i:s",filemtime($file));
			
			//テキストファイル内のテキスト確保
			$fp = fopen($file,'r');
			$text[] = "";
			$NoL = 0;
			while(!feof($fp)){
				$text[$NoL] = fgets($fp);
				$text[$NoL] = str_replace("\r\n", '',$text[$NoL]);
				$NoL++;
			}
			//--------------------------------//
			
			
			//-------------出力部-------------//
			echo "<script type=\"text/javascript\">";
			echo "let win" . $winCnt . "= window.open();";
			echo "win" . $winCnt . ".document.open();";
			echo "win" . $winCnt . ".document.write(\"<html>\");";
			echo "win" . $winCnt . ".document.write(\"<head><title>新しいタブ</title></head>\");";
			echo "win" . $winCnt . ".document.write(\"<body>\");";
			echo "win" . $winCnt . ".document.write(\"<h1 style=color:blue;>ファイル名：" . $name[$winCnt] . "</h1>\");";
			echo "win" . $winCnt . ".document.write(\"<h2>最終更新：" . $timeStanp . "</h2>\");";		
			
			for($i = 0; $i < $NoL; $i++){
				echo "win" . $winCnt . ".document.write(\"<h3>" . $text[$i] . "</h3>\");";
			}
			
			echo "win" . $winCnt . ".document.write(\"</body>\");";
			echo "win" . $winCnt . ".document.write(\"</html>\");";
			echo "win" . $winCnt . ".document.close();";
			echo "</script>";
			//--------------------------------//
			
			//ファイルリンク解除
			fclose($fp);
			
			//ファイル削除
			//unlink($file);
			
			//現在時刻
			echo "現在時刻：" . date("Y-m-d H:i:s") . "<br>";
			
			echo "ファイル名：" . $name[$winCnt] . " を削除しました。" . "<br><br>";
		}

	?>
	</body>
</html>