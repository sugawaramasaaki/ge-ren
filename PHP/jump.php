<?php
	
	//タイムゾーン指定
	date_default_timezone_set('Asia/Tokyo');
	
	//受け取ったhtmlファイルの位置を確保
	//変なの受け取ったら終了
	$path = $argv[1];
	if((preg_match("/(.+http)|(html.+)/",$path)) || (!preg_match("/http.*html/",$path))){
		exit();
	}
	
	//300秒間隔に繰り返し
	while(true){
		
		//ファイル指定
		$dir = "./monitor/";
		$name[] = "";
		
		$winCnt = 0;
			
		//ファイル探査
		foreach(glob($dir . "{*.txt,.txt}", GLOB_BRACE) as $file){
			//ヒット数増加
			$winCnt++;
			
			//---------出力データ準備---------//
			//ファイル名確保
			$name[$winCnt] = basename($file);
			
			//ファイルのタイムスタンプ確保
			$timeStamp = date("Y-m-d H:i:s",filemtime($file));
			
			//テキストファイル内のテキスト確保
			$fp = fopen($file,'r');
			$text = "";
			
			//テキストデータ準備
			while(!feof($fp)){				//ファイルの末尾まで探査
				$text = fgets($fp);
				
				$text = str_replace(PHP_EOL,"^<br^>",$text);	//改行コード置き換え
				$text = str_replace(PHP_EOL," ", "+",$text);	//半角スペース置き換え
				$text = str_replace(PHP_EOL,"  ", "%E3%80%80",$text);	//全角スペース置き換え
				$text = str_replace(PHP_EOL,"	", "%09",$text);	//水平タブ置き換え
			}
			
			//ファイルリンク解除
			fclose($fp);
			
			//ファイル削除
			unlink($file);
			
			/*
			header('Location:http://localhost/GE3A08/%E5%80%8B%E4%BA%BA/Let\'sShooting/PHP/final.htm?
			fileName=' . $name[$winCnt] . '&
			timeStanp=' . $timeStanp . '&
			text=' . 
			for($i = 0; $i < $NoL; $i++){
				echo "win" . $winCnt . ".document.write(\"<h3>" . $text[$i] . "</h3>\");";
			}
			);
			*/
			
			//htmlファイルにデータ送信
			shell_exec("start " . $path . "?TimeStanp=" . $timeStamp . "^&FileName=" . $name[$winCnt] . "^&FileStr=" . $text);
		}
		
		//現在時刻
		echo "現在時刻：" . date("Y-m-d H:i:s") . "<br>";
		
		for($i = 0; $i < $winCnt; $i++){
			echo "ファイル名：" . $name[$i] . " を削除しました。" . "<br><br>";
		}
		//間隔300
		sleep(10);
	}

?>