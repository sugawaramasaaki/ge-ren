<!docktype html>
	<html>
	<head>
	<meta charset="UTF-8">
	<title>ログイン</title>
	</head>
	<body>
	<?php
		define("DSN","mysql:dbname=ge3a_db;host=127.0.0.1");
		define("UN","ge3a");
		define("PS","ge3a");
		
		try{
			//コネクト
			$pdo = new PDO(DSN,UN,PS);
			
			$account = $_POST["account"];
			
			$password = $_POST["passwd"];
			
			$sql = "SELECT * FROM user_tbl WHERE user_account = :account && 
									user_password = PASSWORD(:password)";
			
			$stmt = $pdo->prepare( $sql );
			
			$stmt->bindParam(":account",$account);
			$stmt->bindParam(":password",$password);
			
			$stmt->execute();
			if($stmt->fetch()){
				echo "ログインできました";
			}
			else{
				echo "アカウント名かパスワードが間違っています";
			}
		}
		catch(PDOException $ex){
			die("Error:" . $ex->getMessage());
		}
		
		$pdo = null;
	?>
	</body>
	</html>