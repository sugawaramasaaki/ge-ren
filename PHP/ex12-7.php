<?php
	require_once("Character.php");
	require_once("Enemy.php");
	
	//ダメージ値格納用変数
	static $damage;
	
	//キャラクターのステータス設定
	echo "\nキャラクターのHPを入力してください\n";
	$CharaHP = trim(fgets(STDIN));
	
	echo "キャラクターのMPを入力してください\n";
	$CharaMP = trim(fgets(STDIN));
	
	echo "\n";
	
	$character = new Character($CharaHP,$CharaMP);
	$enemy = new Enemy();
	
	echo "キャラクターの現在のHP・MP\n";
	echo "HP：" . $character->gethp() . "		MP：" . $character->getmp() . "\n";
	
	echo "エネミーの攻撃によるダメージ\n";
	echo $damage = $enemy->attack();
	
	echo "\n";
	
	//ダメージを確保
	$character->setrecentlyDamage($damage);
	
	//ダメージ反映
	$character->hurt();
	
	echo "現在のキャラクターのHP・MP\n";
	echo "HP：" . $character->gethp() . "		MP：" . $character->getmp() . "\n";
	
	
?>