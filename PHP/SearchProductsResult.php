<!doctype html>
	<html>
	<head>
	<meta charset="UTF-8">
	<link rel="stylesheet" href="SearchProductsCSS.css">
	<title>検索結果</title>
	</head>
	<body>
	<h1>下記の商品がヒットしました</h1>
	<div>
	<form action="SearchProductsResult.php" method="POST">
		<select name="sort">
			<option value="NameASC">商品名昇順</option>
			<option value="NameDESC">商品名降順</option>
			<option value="CodeASC">商品コード昇順</option>
			<option value="CodeDESC">商品コード降順</option>
			<option value="PriceASC">価格昇順</option>
			<option value="PriceDESC">価格降順</option>
			<option value="MemoASC">備考昇順</option>
			<option value="MemoDESC">備考降順</option>
		</select>

		<input type="submit" value="リロード">
	</form>
	</div>
	
	<?php
		define("DSN","mysql:dbname=ge3a_db;host=127.0.0.1");
		define("UN","ge3a");
		define("PS","ge3a");
		
		define("SQL","SELECT product_name, product_code, product_price, product_memo FROM products_tbl WHERE 1 = 1");
		define("WORD"," && CONCAT (product_name, product_code, IFNULL(product_memo, \"\")) collate utf8_unicode_ci like :word");
		define("BETWEEN"," && product_price between :lower and :upper ");
		define("SOR","order by :sort");
		
		session_start();
		
		try{
			
			//PDO
			
			//コネクト
			$pdo = new PDO(DSN,UN,PS);
			
			$test = SQL;
			
			//受け取り
			if(isset($_POST["word"])){
				$_SESSION["word"] = $_POST["word"];
			}
			if(isset($_SESSION["word"])){
				//キーワードの空白、タブを%に置換
				$_SESSION["word"] = mb_convert_kana($_SESSION["word"], "KV");
				$_SESSION["word"] = "%" . preg_replace("/( |	|　)+/","%",$_SESSION["word"]) . "%";
				
				$test .= WORD;
			}
			if(isset($_POST["lower"]) && isset($_POST["upper"])){
				$_SESSION["lower"] = $_POST["lower"];
				$_SESSION["upper"] = $_POST["upper"];
			}
			if(isset($_SESSION["lower"]) && isset($_SESSION["upper"])){
				$test .= BETWEEN;
			}
			
			$_SESSION["sort"] = $_POST["sort"];
			
			switch($_SESSION["sort"]){
				case "NameASC":
					$_SESSION["sort"] = "product_name ASC;";
					break;
				case "NameDESC":
					$_SESSION["sort"] = "product_name DESC;";
					break;
				case "CodeASC":
					$_SESSION["sort"] = "product_code ASC;";
					break;
				case "CodeDESC":
					$_SESSION["sort"] = "product_code DESC;";
					break;
				case "PriceASC":
					$_SESSION["sort"] = "product_price ASC;";
					break;
				case "PriceDESC":
					$_SESSION["sort"] = "product_price DESC;";
					break;
				case "MemoASC":
					$_SESSION["sort"] = "product_memo ASC;";
					break;
				case "MemoDESC":
					$_SESSION["sort"] = "product_memo DESC;";
					break;
				
			}
			if(isset($_SESSION["sort"])){
				$test .= SOR;
			}
			$stmt = $pdo->prepare($test);
			
			//バインド
			if(isset($_SESSION["word"])){
				$stmt->bindParam(":word",$_SESSION["word"]);
			}
			if(isset($_SESSION["lower"]) && isset($_SESSION["upper"])){
				$stmt->bindParam(":lower",$_SESSION["lower"]);
				$stmt->bindParam(":upper",$_SESSION["upper"]);
			}
			$stmt->bindParam(":sort",$_SESSION["sort"]);
			
			//実行
			$stmt->execute();
			echo $_SESSION["word"];
			echo $_SESSION["lower"];
			echo $_SESSION["upper"];
			echo $_SESSION["sort"];
			echo $test;
			
			echo "<table border=1>";
			echo "<tr>";
			echo "<td align=\"center\" class=\"blue\">商品名</td>";
			echo "<td align=\"center\" class=\"blue\">商品コード</td>";
			echo "<td align=\"center\" class=\"blue\">値段</td>";
			echo "<td align=\"center\" class=\"blue\">コメント</td>";
			echo "</tr>";
			
			//出力
			while($row = $stmt->fetch()){
				echo "<tr>";
				echo "<td align=\"center\">" . $row[0] . "</td>";
				echo "<td align=\"right\">" . $row[1] . "</td>";
				echo "<td align=\"right\">" . $row[2] . "</td>";
				echo "<td align=\"right\">" . $row[3] . "</td>";
				echo "</tr>";
			}
			echo "</table>";
			
		}
		catch(PDOException $ex){
			unset($_SESSION["word"]);
			unset($_SESSION["lower"]);
			unset($_SESSION["upper"]);
			unset($_SESSION["sort"]);
			die("Error:" . $ex->getMessage());
		}
		
		$pdo = null;
	?>
	</body>
	</html>