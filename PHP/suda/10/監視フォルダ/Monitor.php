<?php
	//タイムゾーンを設定
	date_default_timezone_set('Asia/Tokyo');

	//test.htmlの絶対パス
	$absPath = $argv[1];
	if((preg_match("/(.+http)|(html.+)/", $absPath))
	|| (!preg_match("/http.*html/", $absPath))){
		echo "Sorry, File couldn't found...\n";
		echo "Press the cross on the upper right to finish.";
		exit();
	}

	//300秒監視
	while(true){
		//フォルダ内検索(../monitor/.txt, ../monitor/*.txt)
		$searchDir = "../monitor/";
		$searchFiles = "{.txt,*.txt}";
		$dirArray = glob($searchDir . $searchFiles, GLOB_BRACE);

		//削除対象ファイル数
		$deleteFileCount = count($dirArray);

		//フォルダ内の.txt表示
		foreach($dirArray as $filePath){			
			//タイムスタンプ
			$date = date( "Y/m/d", filemtime($filePath));

			//ファイル名("../monitor/"を省く)
			$fileName = substr($filePath, 11);			

			//ファイルの中身を開く
			$ofile = fopen($filePath, "r");

			//ファイルの中身を取得
			$fileStr = "";
			$lines = 0;
			while($line = fgets($ofile)){
				//エンコード
				$line = str_replace(PHP_EOL, "^<br^>", $line);		//改行
				$line = str_replace(" ", "+", $line);				//半角スペース
				$line = str_replace("　", "%E3%80%80", $line);		//全角スペース
				$line = str_replace("	", "%09", $line);			//水平タブ

				//文字追加
				$row = "";
				$lines++;
				if($lines < 10)$row = "+" . $lines . "^|";
				else if($lines < 100)$row = "" . $lines . "^|";
				//else if($lines < 1000)$row = "+" . $lines . "^|";
				//else if($lines < 10000)$row = $lines . "^|";
				$fileStr .= $row . str_replace(PHP_EOL, "^<br^>", $line);
			}

			//ファイルを閉じる
			fclose($ofile);

			//HTMLを開く
			shell_exec("start " . $absPath . "?TimeStamp=" . $date . "^&FileName=" . $fileName . "^&FileStr=" . $fileStr);

			//ファイル削除
			//unlink($filePath);
		}

		//ファイルの削除件数報告
		echo "Monitor.php>" . date("Y/m/d H:i:s") . "に " . $deleteFileCount . " 件のファイルを削除しました。\n";

		//300秒待機
		sleep(10);
	}
?>