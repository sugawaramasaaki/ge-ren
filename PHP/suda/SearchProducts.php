<!doctype html>
<html>
	<head>
		<meta charset="UTF-8">
		<title>商品検索ページ</title>

		<script type="text/javascript">
			function inputClear(){
				input.key.value = "";
			}
			function inputCheck(){
				if(parseInt(input.min.value) >= 0 && parseInt(input.max.value) == "")return true;
				if(parseInt(input.min.value) == "" && parseInt(input.max.value) >= 0)return true;
				if(parseInt(input.min.value) > parseInt(input.max.value) || 
				parseInt(input.min.value) < 0 || parseInt(input.max.value) < 0){
					alert("価格の設定に誤りがあります");
					return false;
				}else{
					return true;
				}
			}
		</script>
	</head>
	<body bgcolor="#7effd6">
		<h1>商品検索</h1>
		<div style="padding:10px; margin-bottom:10px; border:1px solid #333333; background-color: #7effff;">
			<form action="SearchProductsResult.php" method="post" name="input">
				<p><label>フリーワード<br><input type="text" name="key" style="width:100%" placeholder="キーワードを入力"></label>
				<button type="button" onclick="inputClear()">クリア</button></p>
				<!--<label><input type="radio" name="search" checked="checked" value="all">全て含む</label>
				<label><input type="radio" name="search" value="part">いずれかを含む</label>-->
				<p><label>価格<br><input type="number" name="min" placeholder="0">円</label> ～ <label><input type="number" name="max" placeholder="999999">円</label></p>
				<p><label>検索方法:<select name="order">
					<option value=0>未設定</option>
					<option value=1>名前の昇順</option>
					<option value=2>名前の降順</option>
					<option value=3>商品コードの昇順</option>
					<option value=4>商品コードの降順</option>
					<option value=5>価格の安い順</option>
					<option value=6>価格の高い順</option>
					<option value=7>商品説明の昇順</option>
					<option value=8>商品説明の降順</option>
				</select></label></p>
				<input type="submit" style="width:100%;background-color:#ffff8a;" value="🔍検索" onclick="return inputCheck()">
			</form>
		</div>
	</body>
</html>