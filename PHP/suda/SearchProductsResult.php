<!doctype html>
<html>
	<head>
		<meta charset="UTF-8">
		<title>商品検索結果ページ</title>

			<script type="text/javascript">
			function inputClear(){
				input.key.value = "";
			}
			function inputChange(){
				if(inputCheck()){
					input.submit();
				}
			}
			function inputCheck(){
				if(parseInt(input.min.value) >= 0 && parseInt(input.max.value) == "")return true;
				if(parseInt(input.min.value) == "" && parseInt(input.max.value) >= 0)return true;
				if(parseInt(input.min.value) > parseInt(input.max.value) || 
				parseInt(input.min.value) < 0 || parseInt(input.max.value) < 0){
					alert("価格の設定に誤りがあります");
					return false;
				}else{
					return true;
				}
			}
		</script>
	</head>
	<body bgcolor="#7effd6">
		<h1>⚔商品検索結果⚔</h1>

		<?php
			define("DSN", "mysql:dbname=ge3a_db;host=127.0.0.1");
			define("DBUSER", "ge3a");
			define("DBPASS", "ge3a");
			define("SELECT", "SELECT product_name, product_code, product_price, product_memo ");
			define("SQL", "SELECT product_name, product_code, product_price, product_memo 
					FROM products_tbl
					WHERE CONCAT(product_name, product_code, IFNULL(product_memo, \"\")) COLLATE utf8_unicode_ci LIKE REPLACE(REPLACE( :keyword , 'ﾞ', ''), 'ﾟ', '') AND 
					product_price BETWEEN :priceMin AND :priceMax");
			
			echo "<div style=\"padding: 10px; margin-bottom: 10px; border: 1px solid #333333; background-color: #7effff;\">";
				echo "<details><summary>商品検索</summary>";
					echo "<form action=\"SearchProductsResult.php\" method=\"post\" name=\"input\">";
						echo "<p><label>フリーワード<br><input type=\"text\" name=\"key\" style=\"width:100%\" placeholder=\"キーワードを入力\" value=\"" . $_POST["key"] ."\"></label>";
						echo "<button type=\"button\" onclick=\"inputClear()\">クリア</button></p>";
						/*if($_POST["search"] == "all")
						{
							echo "<label><input type=\"radio\" name=\"search\" checked=\"checked\" value=\"all\">全て含む</label>";
							echo "<label><input type=\"radio\" name=\"search\" value=\"part\">いずれかを含む</label>";
						}
						if(	$_POST["search"] == "part")
						{			 
							echo "<label><input type=\"radio\" name=\"search\" value=\"all\">全て含む</label>";
							echo "<label><input type=\"radio\" name=\"search\" checked=\"checked\" value=\"part\">いずれかを含む</label>";
						}*/
						echo "<p><label>価格<br><input type=\"number\" name=\"min\" placeholder=\"0\" value=" . $_POST["min"] ." >円</label> ～ <label><input type=\"number\" name=\"max\"  placeholder=\"999999\" value=" . $_POST["max"] .">円</label></p>";
						echo "<p><label>検索方法:<select name=\"order\" onchange=\"inputChange()\">";
						echo "<option value=0 "; if($_POST["order"] == "0") echo "selected=\"selected\""; echo ">未設定</option>";
						echo "<option value=1 "; if($_POST["order"] == "1") echo "selected=\"selected\""; echo ">名前の昇順</option>";
						echo "<option value=2 "; if($_POST["order"] == "2") echo "selected=\"selected\""; echo ">名前の降順</option>";
						echo "<option value=3 "; if($_POST["order"] == "3") echo "selected=\"selected\""; echo ">商品コードの昇順</option>";
						echo "<option value=4 "; if($_POST["order"] == "4") echo "selected=\"selected\""; echo ">商品コードの降順</option>";
						echo "<option value=5 "; if($_POST["order"] == "5") echo "selected=\"selected\""; echo ">価格の安い順</option>";
						echo "<option value=6 "; if($_POST["order"] == "6") echo "selected=\"selected\""; echo ">価格の高い順</option>";
						echo "<option value=7 "; if($_POST["order"] == "7") echo "selected=\"selected\""; echo ">商品説明の昇順</option>";
						echo "<option value=8 "; if($_POST["order"] == "8") echo "selected=\"selected\""; echo ">商品説明の降順</option>";
						echo "</select></label></p>";
						echo "<input type=\"submit\" style=\"width:100%;background-color:#ffff8a;\" value=\"🔍再検索\" onclick=\"return inputCheck()\">";
					echo "</form>";
				echo "</details>";
			echo "</div>";

			try{
				//PDOクラスのインスタンス生成とDB接続
				$pdo = new PDO(DSN, DBUSER, DBPASS);

				//POSTから受けたっと情報を格納
				$keyword = "%" . preg_replace("/( |　|	)+/", "%", $_POST["key"]) . "%";
				$priceMin = $_POST["min"];
				$priceMax = $_POST["max"];
				$order = $_POST["order"];

				//値未入力の対処
				if($priceMin == "")$priceMin = 0;
				if($priceMax == "")$priceMax = 99999999;
				if($order == "")$order = 0;

				//SQL登録
				$array = array(
				"product_id;",
				"product_name ASC;",
				"product_name DESC;",
				"product_code ASC;",
				"product_code DESC;",
				"product_price ASC;",
				"product_price DESC;",
				"product_memo ASC;",
				"product_memo DESC;");
				$stmt = $pdo->prepare(SQL . " ORDER BY " . $array[$order]);

				//バインド
				$stmt->bindParam(":keyword", $keyword);
				$stmt->bindParam(":priceMin", $priceMin);
				$stmt->bindParam(":priceMax", $priceMax);

				//実行
				$stmt->execute();

				//結果
				echo "<table border=1>";
				echo "<tr>";
				echo "<th align=\"center\">商品名</th>";
				echo "<th align=\"center\">商品コード</th>";
				echo "<th align=\"center\">商品価格</th>";
				echo "<th align=\"center\">商品説明</th>";
				echo "</tr>";
				while($row = $stmt->fetch()){
					echo "<tr>";
					echo "<td>" . $row[0] . "</td>";
					echo "<td>" . $row[1] . "</td>";
					echo "<td>" . $row[2] . "</td>";
					echo "<td>" . $row[3] . "</td>";
					echo "</tr>";
				}
				echo "</table>";
			}catch(PDOException $ex){
				//死亡
				die( "Error;" . $ex->getMessage());
			}finally{
				//切断
				$pdo = null;
			}
		?>
	</body>
</html>