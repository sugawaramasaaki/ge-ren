#include "Text.h"
#include <vector>
#include "Global.h"

namespace Text
{
	struct TextFormat
	{
		IDWriteTextFormat* pTextFormat_;
		float fontSize_;
		std::string fontType_;
	};

	struct TextColor
	{
		ID2D1SolidColorBrush* pSolidBrush_;
		D2D1::ColorF::Enum tectColor_;
	};

	struct TextData
	{
		std::wstring text_;
		Point point_;
		TextFormat* format_;
		TextColor* color_;
	};

	std::vector<TextData*> datas_;
	std::vector<TextFormat*> formats_;
	std::vector<TextColor*> colors_;

	std::vector<bool> isDraw_;

	std::wstring StringToWString(std::string str)
	{
		// SJIS → wstring
		int bufferSize = MultiByteToWideChar(CP_ACP, 0, str.c_str()
			, -1, (wchar_t*)NULL, 0);

		// バッファの取得
		wchar_t* cpUCS2 = new wchar_t[bufferSize];

		// SJIS → wstring
		MultiByteToWideChar(CP_ACP, 0, str.c_str(), -1, cpUCS2
			, bufferSize);

		// stringの生成
		std::wstring ret(cpUCS2, cpUCS2 + bufferSize - 1);

		// バッファの破棄
		delete[] cpUCS2;

		// 変換結果を返す
		return(ret);
	}

	TextFormat* CreateFormat(std::string fontType, float fontSize)
	{
		//フォーマット探し
		TextFormat* pFormat = nullptr;
		for (TextFormat* format_ : formats_)
		{
			if (format_->fontSize_ == fontSize && format_->fontType_ == fontType)
			{
				pFormat = format_;
			}
		}

		//新規作成
		if (!pFormat)
		{
			pFormat = new TextFormat;
			pFormat->fontSize_ = fontSize;
			pFormat->fontType_ = fontType;

			Direct2D::pDWriteFactory->CreateTextFormat(
				std::wstring(fontType.begin(), fontType.end()).c_str(),
				nullptr,
				DWRITE_FONT_WEIGHT_NORMAL,
				DWRITE_FONT_STYLE_NORMAL,
				DWRITE_FONT_STRETCH_NORMAL,
				fontSize,
				L"",
				&pFormat->pTextFormat_
			);
			pFormat->pTextFormat_->SetTextAlignment(DWRITE_TEXT_ALIGNMENT_LEADING);
		}

		return pFormat;
	}

	TextColor* CreateColor(D2D1::ColorF::Enum color)
	{
		//カラー探し
		TextColor* pColor = nullptr;
		for (TextColor* color_ : colors_)
		{
			if (color_->tectColor_)
			{
				pColor = color_;
			}
		}

		//新規作成
		if (!pColor)
		{
			pColor = new TextColor;
			pColor->tectColor_ = color;

			Direct2D::pRT->CreateSolidColorBrush(D2D1::ColorF(color), &pColor->pSolidBrush_);
		}
		return pColor;
	}
}

int Text::SetText(std::string text, std::string fontType, Point point, float fontSize, D2D1::ColorF::Enum color)
{
	TextFormat* pFormat = CreateFormat(fontType, fontSize);

	TextColor* pColor = CreateColor(color);

	TextData* pData = new TextData;
	pData->format_ = pFormat;
	pData->color_ = pColor;
	pData->point_ = point;
	pData->text_ = StringToWString(text);

	datas_.push_back(pData);
	isDraw_.push_back(true);

	return datas_.size() - 1;
}

void Text::ChangeText(int handle, std::string text)
{
	datas_[handle]->text_ = std::wstring(text.begin(), text.end());
}

void Text::ChangePoint(int handle, Point point)
{
	datas_[handle]->point_ = point;
}

void Text::ChangeColor(int handle, D2D1::ColorF::Enum color)
{
	datas_[handle]->color_ = CreateColor(color);
}

void Text::ChangeFormat(int handle, std::string fontType, float fontSize)
{
	datas_[handle]->format_ = CreateFormat(fontType, fontSize);
}

D2D1::ColorF::Enum Text::ConvertColor(unsigned char red, unsigned char gleen, unsigned char blue)
{
	int iRed = (int)red << 16;
	int iGreen = (int)gleen << 8;
	int iBlue = (int)blue;

    return (D2D1::ColorF::Enum)(iRed + iGreen + iBlue);
}

void Text::Visible(int handle)
{
	isDraw_[handle] = true;
}

void Text::Invisible(int handle)
{
	isDraw_[handle] = false;
}

void Text::Draw()
{
	//テキストの描画
	for (int i = 0; i < datas_.size(); i++)
	{
		if (isDraw_[i])
		{
			TextData* data = datas_[i];

			Direct2D::pRT->DrawText(
				data->text_.c_str(),
				data->text_.size(),
				data->format_->pTextFormat_,
				D2D1::RectF(data->point_.X, data->point_.Y, 1600, 1200),
				data->color_->pSolidBrush_,
				D2D1_DRAW_TEXT_OPTIONS_NONE
			);
		}
	}
}

void Text::Release()
{
	//テキストデータ解放
	for (TextData* data : datas_)
	{
		SAFE_DELETE(data);
	}
	datas_.clear();

	//色データの解放
	for (TextColor* color : colors_)
	{
		SAFE_RELEASE(color->pSolidBrush_);
		SAFE_DELETE(color);
	}
	colors_.clear();

	//書式データの解放
	for (TextFormat* format : formats_)
	{
		SAFE_RELEASE(format->pTextFormat_);
		SAFE_DELETE(format);
	}
	formats_.clear();

	//描画判断のクリア
	isDraw_.clear();
}
