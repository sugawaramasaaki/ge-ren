<?php
	
	//電話番号チェック用関数
	//引数：電話番号（文字列）
	function isTel($str){
		
		//半角に変換
		$str = mb_convert_kana($str,'n');
		
		//携帯電話用正規表現
		$MobilePhoneNumber = '^0[7-9]0[ -]?\d{4}[ -]?\d{4}$';
		
		//固定電話用正規表現
		$FixedlinePhonNumber = '^0\d{1}[ -]?\d{4}[ -]?\d{4}$|^0\d{2}[ -]?\d{3}[ -]?\d{4}$|^0\d{3}[ -]?\d{2}[ -]?\d{4}$|^0\d{4}[ -]?\d{1}[ -]?\d{4}$';
		
		//代表的なフリーダイヤル
		$FreedialNumber = '^0120[ -]?\d{3}[ -]?\d{3}$|^0800[ -]?\d{3}[ -]?\d{4}$';
		
		//文字列チェック部分
		if(preg_match('/' . $MobilePhoneNumber . '|' . $FixedlinePhonNumber . '|' . $FreedialNumber . '/', $str)) {
			 return "true";
		}
		else { return "false"; }
	}
	
	
	//-----------------------------------------------------------------------------------------------------------//
	
	
	//メールアドレスチェック用関数
	//引数：メールアドレス（文字列）
	function isMail($str){
		
		//連続文字制限
		//説明：「.」「_」が2回以上連続して登場しない、アドレスの末尾が記号の連続ではない
		$Continuous = '^(?!.*\.{2,})(?!.*_{2,})(?!.*[\$\=\?\^`\{\}~#@-]{2,}$)';
		
		//使用不可判定
		//説明：スペースが1文字も入っていない、使用許可している文字以外が存在しない
		$Usageprohibited = '(?!.*[^\w\.\$\=\?\^`\{\}~#@-])';
		
		//メールアドレスのルール
		//説明：1文字目が記号以外、「@」の直前に記号がない、アドレスの末尾が「.」、「 」で終わっていない
		$MailRule = '(?=[\da-zA-Z]([\w\.\$=\?\^`\{\}~#-]|[^\._\$\=\?\^`\{\}~#-])*@[\w\.\$=\?\^`\{\}~#-]*[^\.]$)';
		
		//長さ制限
		//説明：アドレスの長さ256以内であること
		$Length = '(?=.{3,256}$)';
		
		//文字列チェック部分
		if(preg_match('/' . $Continuous . $Usageprohibited . $MailRule . $Length . '/', $str)){
			return "true";
		}
		else { return "false"; }
	}
	
	
	//-----------------------------------------------------------------------------------------------------------//
	
	
	//電話番号とメールアドレスどちらをチェックしたいか
	$FunctionArray_ = array("isTel","isMail");
	
	echo "電話番号をチェックしたいなら０を、メールアドレスをチェックしたいなら１を入力してください。\n";
	$CheckNumber_ = trim(fgets(STDIN));
	
	if($CheckNumber_ == 0)
	{
		//チェックしたい電話番号を入力
		echo "チェックしたい電話番号を入力してください。";
		$CheckTel = trim(fgets(STDIN));
		
		echo $FunctionArray_[$CheckNumber_]($CheckTel) . "\n";
	}
	else if($CheckNumber_ == 1)
	{
		//チェックしたいメールアドレスを入力
		echo "チェックしたいメールアドレスを入力してください。\n";
		$CheckMail = trim(fgets(STDIN));
		
		echo $FunctionArray_[$CheckNumber_]($CheckMail) . "\n";
	}
	else
	{
		echo "Error\n";
	}

?>